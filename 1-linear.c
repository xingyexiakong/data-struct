/*************************************************************************
	> File Name: 1-linear.c
	> Author: 
	> Mail: 
	> Created Time: Sat 27 Jan 2024 09:43:32 PM CST
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct vector{
    int *data;
    int size;
    int len;
}Vec;

Vec *init(){
    Vec *tmp = (Vec *)malloc(sizeof(Vec));
    tmp->size = 5;
    tmp->len = 0;
    tmp->data = (int *)malloc(sizeof(int) * tmp->size);
    
    printf("inital successfully!\n");
    return tmp;
    
}

int free_vec(Vec *v){
    if(v){
    free(v->data);
    free(v);
    printf("delete successfully!\n");
    return 1;
    }
    return 0;
}

int expand(Vec *v){
    if(!v){
        return 0;
    }
    v->data = (int *)realloc(v->data ,sizeof(int) * v->size * 2);
    v->size *= 2;
    printf("expand cap successfully!\n");
    return 1;
    
}

int insert(Vec *v, int val, int index){
    if(!v){
        return 0;
    }
    if(index < 0 || index > v->size){
        return 0;
    }
    if(v->data[v->size - 1]){
        expand(v);
    }
    if(!v->data[index - 1]){
        v->data[index - 1] = val;
        return 1;
    }
    memcpy(v->data + index  , v->data + index - 1, sizeof(int) * (v->size - index));
    v->data[index - 1] = val;
    v->len++;
    printf("insert number successfully!\n");
    return 1;
}

int erase(Vec *v, int idx){
    if(!v){
        return 0;
    }
    if(idx < 0 || idx > v->size - 1){
        return 0;
    }
    if(!v->data[idx - 1]){
        printf("there is not any data!\n");
        return 0;
    }
    memcpy(v->data + idx - 1, v->data + idx, sizeof(int) * v->size - idx);
    v->len--;
    v->data[v->size - 1] = 0; //初始化内存数据
    return 1;
}

void show(Vec *v){
    for(int i = 0; i < v->size; i++){
        printf("%d ", v->data[i]);
    }
    printf("\n");
}

int main(){

    Vec *v = init();
    int a, b, c;
    while(~scanf("%d", &a)){
        if(a == 1){
            scanf("%d %d", &b, &c);
            insert(v, c, b);
        }
        else if(a == 2){
            scanf("%d", &b);
            erase(v, b);
        }
        else{
            free_vec(v);
            printf("free the vector successfully!\n");
            return 0;
        }
        show(v);
    }

    return 0;
}
