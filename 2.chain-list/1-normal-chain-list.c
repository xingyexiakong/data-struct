/*************************************************************************
	> File Name: 1-normal-chain-list.c
	> Author: 
	> Mail: 
	> Created Time: Sun 28 Jan 2024 12:18:03 PM CST
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>


typedef struct Node{
    int val;
    struct Node *next;
}node;

typedef struct hain_list{
    node *head;
    int len;
}list;

void show(list *l);
node *init_node(){
    node *n = (node *)malloc(sizeof(node));
    n->val = 0;
    n->next = NULL;
    return n;
}

list *init_list(){
    list *l = (list *)malloc(sizeof(list));
    l->head = init_node();
    l->len = 0;
    return l;
}

int free_list(list *l){
    if(!l){
        return 0;
    }
    node *p;
    p = l->head;
    while(p){
        l->head = l->head->next;
        free(p);
        p = l->head;
    }
    free(l);
    return 1;
}

int insert_byhead(list *l, node *n){
    if(!l){
        return 0;
    }
    n->next = l->head;
    l->head = n;
    l->len++;
    return 1;
}

int insert_byidx(list *l, int idx, node *n){
    if(!l){
        return 0;
    }

    if(idx < 0 || idx > l->len){
        return 0;
    }
    if(!idx){
       n->next = l->head;
        l->head = n;
        l->len++;
        return 1;
    }
    node *q = l->head;
    while(--idx){
        q = q->next;
    }
    n->next = q->next;
    q->next = n;
    l->len++;
    return 1;

}
int erase(list *l,int idx){
    if(!l){
        return 0;
    }
    if(idx < 0 || idx > l->len){
        return 0;
    }
    node *q;
    node *n = l->head;
    if(!idx){
        l->head = l->head->next;
        free(n);
        l->len--;
        return 1;
    }
    while(--idx){
        n = n->next;
    }
    q = n->next;
    n->next = n->next->next;
    free(q);
    return 1;

}
int reverse_list(list *l){
    
    node *t, *k;
    t = l->head;
    l->head = NULL;
    l->len = 0;
    while(t){
        
        k = t;
        t = t->next;
        insert_byhead(l,k);
    }
    return 1;
}

void show(list *l){
    node *p;
    p = l->head;
    while(p){
        printf("%d", p->val);
        p->next && printf("->");
        p = p->next;
    }
    printf("\n");
}

int main(){
    
    list *l = init_list();
    int a, b ,c;
    while(~scanf("%d" ,&a)){
        if(a == 1){
            scanf("%d %d",&b, &c);
            node *t = init_node();
            t->val = c;
            insert_byidx(l, b, t);
        }
        else if(a == 2){
            scanf("%d", &b);
            erase(l, b);
        }
        else if(a == 3){
            reverse_list(l);
        }

        else{
            free_list(l);
        }
        show(l);
    }
    return 1;
}
