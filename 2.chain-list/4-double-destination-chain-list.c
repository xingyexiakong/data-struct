/*************************************************************************
	> File Name: 1-normal-chain-list.c
	> Author: 
	> Mail: 
	> Created Time: Sun 28 Jan 2024 12:18:03 PM CST
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
    int val;
    struct Node *next;
    struct Node *prex;
}node;

typedef struct hain_list{
    node *head;
    int len;
}list;

node *init_node(){
    node *n = (node *)malloc(sizeof(node));
    n->val = -1;
    n->next = NULL;
    return n;
}

list *init_list(){
    list *l = (list *)malloc(sizeof(list));
    l->head = init_node();
    l->len = 0;
    return l;
}

int free_list(list *l){
    if(!l){
        return 0;
    }
    node *p;
    p = l->head;
    while(p){
        l->head = l->head->next;
        free(p);
        p = l->head;
    }
    free(l);
    return 1;
}

int insert_byhead(list *l, node *n){
    if(!l){
        return 0;
    }
    n->next = l->head->next;
    l->head->next->prex = n;
    l->head->next = n;
    n->prex = l->head;
    l->len++;
    return 1;
}

int insert_byidx(list *l, int idx, node *n){
    if(!l){
        return 0;
    }
    if(idx < 0 || idx > l->len){
        return 0;
    }
    node *t = l->head;
    while(idx--){
        t = t->next;
    }
    n->next = t->next;
    t->next->prex = n;
    t->next = n;
    n->prex = t;
    l->len++;
    return 1;

}
int erase(list *l,int idx){
    if(!l){
        return 0;
    }
    if(idx < 0 || idx > l->len){
        return 0;
    }
    node *t = l->head;
    while(idx--){
        t = t->next;
    }
    node *q;
    q = t->next;
    t->next = t->next->next;
    l->len--;
    free(q);
    return 1;

}

void show(list *l){
    node *p;
    p = l->head;
    printf("HEAD:");
    while(p){
        
        printf("%d", p->val);
        p->next && printf("->");
        p = p->next;
    }
    printf("\n");
}

int main(){
    
    list *l = init_list();
    int a, b ,c;
    while(~scanf("%d" ,&a)){
        if(a == 1){
            scanf("%d",&b);
            node *t = init_node();
            t->val = b;
            insert_byhead(l, t);
        }
        else if(a == 2){
            scanf("%d", &b);
            erase(l, b);
        }
        else{
            free_list(l);
        }
        show(l);
    }
    return 1;
}
