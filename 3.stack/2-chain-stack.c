/*************************************************************************
	> File Name: 2-chain-stack.c
	> Author: 
	> Mail: 
	> Created Time: Tue 30 Jan 2024 09:33:45 AM CST
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
    int val;
    struct Node *next;
}node;

typedef struct stack{
    node *head;
    int size;
}stack;

node *init_node(int i){
    node *n = (node *)malloc(sizeof(node));
    n->val = i;
    n->next = NULL;
    return n;
}

stack *init_stack(){
    stack *s = (stack *)malloc(sizeof(stack));
    s->head = init_node(-1);
    s->size = 0;
    return s;
}

int free_stack(stack *s){
    if(!s){
        return 0;
    }
    node *t =s->head;
    while(s->head){
        s->head = s->head->next;
        free(t);
        t = s->head;
    }
    return 1;
}

int push(stack *s, int val){
    if(!s){
        return 0;
    }
    node *t = init_node(val);
    t->next = s->head->next;
    s->head->next = t;
    s->size++;
    return 0;
}

int pop(stack *s){
    if(!s){
        return 0;
    }
    if(!s->head->next){
        return 0;
    }
    node *t = s->head->next;
    s->head->next = t->next;
    int a = t->val;
    free(t);
    s->size--;
    return a;
}

void show(stack *s){
    if(!s){
        return ;    
    }
    node *t = s->head;
    while(t->next){
        t = t->next;
        printf("%d", t->val);
        t->next && printf(" ");
    }
    printf("\n");
    return ;
}

int main(){
    
    stack *s = init_stack();
    int a, b;
    while(~scanf("%d", &a)){
        if(a == 1){
            scanf("%d", &b);
            push(s, b);
        }
        else if(a == 2){
            b = pop(s);
            printf("弹出的栈顶数据为：%d, 当前长度:%d\n", b, s->size);
        }
        else{
            free_stack(s);
            return 0;
        }
        show(s);
    }

    return 0;
}
