/*************************************************************************
	> File Name: 1-sequence-stack.c
	> Author: 
	> Mail: 
	> Created Time: Tue 30 Jan 2024 08:24:45 AM CST
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>

int expand();

typedef struct stack{
    int *data;
    int head;
    int cap;
}stack;

stack *init_stack(){
    stack *s = (stack *)malloc(sizeof(stack));
    s->cap = 5;
    s->head = 0;
    s->data = (int *)malloc(sizeof(int) * s->cap);
    return s;
}

int push(stack *s, int n){
    if(!s){
        return 0;
    }
    if(s->head == s->cap - 1){
        expand(s);
    }
    s->data[s->head] = n;
    s->head++;
    return 1;
}

int pop(stack *s){
    if(!s){
        return 0;
    }
    if (s->head == 0){
        return 0;
    }
    s->head--;
    return s->data[s->head];
}

int expand(stack *s){
    s->cap *= 2;
    s->data = (int *)realloc(s->data ,sizeof(int) * s->cap);
    printf("expand successfully!\n");
    return 1;
}

int free_stack(stack *s){
    if(!s){
        return 0;
    }
    free(s->data);
    free(s);
    return 1;
}

void show(stack *s){
    for(int i = 0; i < s->head; i++){
        i && printf(" ");
        printf("%d",s->data[i]);
    }
    printf("\n");
}

int main(){
    stack *s = init_stack();
    int a, b, c;
    while(~scanf("%d", &a)){
        if(a == 1){
            scanf("%d", &b);
            push(s, b);
        }
        else if(a == 2){
            printf("弹出元素为:%d\n", pop(s));
        }
        else{
            free_stack(s);
            return 1;
        }
        show(s);
    }

    return 0;
}
