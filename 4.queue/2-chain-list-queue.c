/*************************************************************************
	> File Name: 1-sequence-queue.c
	> Author: 
	> Mail: 
	> Created Time: Wed 31 Jan 2024 08:41:55 AM CST
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>


void show();
typedef struct Node{
    int val;
    struct Node *next;
}node;

typedef struct Queue{
    node *head;
    node *tail;
    int len;
}queue;

node *init_node(int val){
    node *n = (node *)malloc(sizeof(node));
    n->next = NULL;
    n->val = val;
    return n;
}

queue *init_queue(){
    
    queue *q = (queue *)malloc(sizeof(queue));
    q->head = init_node(-1);
    q->tail = init_node(-1);
    q->len = 0;
    q->head->next = q->tail;
    q->tail->next = q->head;
    return q;
}
int free_queue(queue *q){
    if(!q){
        return 0;
    }
    node *n = q->head;
    while(n){
        q->head = n->next;
        free(n);
        n = q->head;
    }
    free(q);
    return 1;
}


int insert(queue *q, int t){
    if(!q){
        return 0;
    }
    node *new = init_node(t);
    new->next = q->tail;
    q->tail->next->next = new;
    q->tail->next = new;
    q->len++;
    return 1;
}

int pop(queue *q){
    if(!q || !q->len){
        return 0;
    }
    int ans = q->head->next->val;
    node *n = q->head->next;
    q->head->next = n->next;
    free(n);
    q->len--;
    if(!q->len){
        q->tail->next = q->head;
    }
    return ans;
}

void show(queue *q){
    if(!q){
        return ;
    }
    node *n = q->head->next;
    
    while(n && n!= q->tail){
        printf("%d", n->val);
        n = n->next;
        n->next && printf("->");
    }
    printf("\n");
}
int main(){
    queue *q = init_queue();
    int a, b, c;
    while(~scanf("%d", &a)){
        if(a == 1){
            scanf("%d", &b);
            int state = insert(q, b);
            printf("state:%d the inserted node is: %d, and the len is %d \n",state, b ,q->len);
        }
        else if(a == 2){
            c = pop(q);
            printf("the popped node is:%d, and the len is %d\n", c, q->len);

        }
        else{
            free_queue(q);
        }
        show(q);
    }
}
