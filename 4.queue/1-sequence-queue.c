/*************************************************************************
	> File Name: 1-sequence-queue.c
	> Author: 
	> Mail: 
	> Created Time: Wed 31 Jan 2024 08:41:55 AM CST
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>

typedef struct Queue{
    int *data;
    int head, tail;
    int size;
    int len;
}queue;

queue *init_queue(){
    queue *q = (queue *)malloc(sizeof(queue));
    q->size = 5;
    q->data = (int *)malloc(sizeof(int) * q->size);
    q->head = 0;
    q->tail = 1;
    q->len = 0;
    return q;
}

int free_queue(queue *q){
    if(!q){
        return 0;
    }
    free(q->data);
    free(q);
    return 1;
}

int expand(queue *q){
    if(!q){
        return 0;
    }
    q->size *= 2;
    int *ans = (int *)malloc(sizeof(int) * q->size);
    for(int i = 1; i <= q->len; i++){
        ans[i] = q->data[(q->head+i) % (q->size/2) ];
    }
    q->head = 0;
    q->tail = q->head + q->len + 1;
    free(q->data);
    q->data = ans;
    
    return 1;
}

int insert(queue *q, int t){
    if(!q){
        return 0;
    }
    if(q->head % q->size == q->tail % q->size){
        expand(q);
        printf("expand capcaity successfully!\n");
    }
    q->data[q->tail % q->size] = t;
    q->tail++;
    q->len++;
    return 1;
}

int pop(queue *q){
    if(!q || !q->len){
        return 0;
    }
  /*  if(q->head % q->size == q->tail % q->size){
        return 0;//the Queue has been full！
    }*/
    q->head++;
    q->len--;
    return q->data[q->head];
}

void show(queue *q){
    if(!q){
        return ;
    }
    int i = q->head;
    int t = q->len;
    while(t--){
        printf("%d ",q->data[++i % q->size]);
    }
    
    printf("\n");
}

int main(){
    queue *q = init_queue();
    int a, b, c;
    while(~scanf("%d", &a)){
        if(a == 1){
            scanf("%d", &b);
            int state = insert(q, b);
            printf("state:%d the inserted element is: %d, and the size is %d \n",state, b ,q->size);
        }
        else if(a == 2){
            c = pop(q);
            printf("the popped element is:%d, and the size is %d\n", c, q->size);

        }
        else{
            free_queue(q);
        }
        show(q);
    }
    return 0;
}
